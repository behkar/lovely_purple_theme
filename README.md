# A lovely purple theme for [Visual Studio Code](http://code.visualstudio.com)

<img alt="icon" src="https://codeberg.org/behkar/lovely_purple_theme/raw/branch/master/images/lovely_purple_theme_icon.png" width="150px" height="150">


See the [CHANGELOG](CHANGELOG.md) for the latest changes.

**Overview**


<img alt="overview" src="https://codeberg.org/behkar/lovely_purple_theme/raw/branch/master/images/lovely_purple_theme_windows_default.png" width="550px" height="300"/><img alt="overview" src="https://codeberg.org/behkar/lovely_purple_theme/raw/branch/master/images/lovely_purple_theme_linux_default.png" width="550px" height="300"/><img alt="overview" src="https://codeberg.org/behkar/lovely_purple_theme/raw/branch/master/images/lovely_purple_theme_linux_2.png" width="550px" height="300"/>

## Installation via VScode Extensions

1. Open **Extensions** sidebar panel and click section :  `View --> Extensions`
2. Search this --> `Lovely Purple Theme`
3. Click **Install** and then click **Reload**
4. File > Preferences > Color Theme > **Lovely Purple Theme**

**Optional**: Use the recommended settings below for best experience

## Recommended Settings

```json
{
  "editor.cursorBlinking": "smooth",
  "editor.cursorStyle": "line",
  "editor.cursorWidth": 2,
  "editor.fontFamily": "Cascadia Code",
  "editor.fontLigatures": true,
  "editor.fontSize": 13,
  "editor.fontWeight": "450",
  "editor.letterSpacing": 0.7,
  "editor.lineHeight": 22,
  "editor.renderWhitespace": "trailing",
  "explorer.compactFolders": false,
  "files.trimTrailingWhitespace": true
}
```
enjoy!

## contact with me (Mahdi Behkar):
email :ubuntulove74@gmail.com </br>
telegram id:
https://t.me/Mahtabdi1374
